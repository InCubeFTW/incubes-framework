README
======

What is D.A.F.E.F.?
-----------------

D.A.F.E. Framework is a PHP 5.4+ web framework. It is written with speed and
flexibility in mind. It allows developers to build better and easy to maintain
websites with PHP. It is also known as the most lightiest, easiest and fastest framework for php.

Requirements
------------

D.A.F.E Framework is only supported on PHP 5.4 and up.
Be warned that we are targeting newest PHP version.
PDO Supported by PHP (MySQL drivers)

Installation
------------

1. Download the zip.
2. Unzip to your public_html folder.
3. http://yoursite.com/index.php :)
4. PDO Connections are set in app/bootstrap.php

Contributing
------------

D.A.F.E. Framework is an open source project by RageZONE drunk developers.


Documentation
-------------

PHP OOP http://php.net/manual/en/language.oop5.php
MySQL PDO http://wiki.hashphp.org/PDO_Tutorial_for_MySQL_Developers

PDO Example of usage in this framework:
try
{
	$stmt = bootstrap::$dbh->query("SELECT * FROM test");
	$fuu = $stmt->fetchAll(PDO::FETCH_ASSOC);
	var_dump($fuu);
}
catch(PDOException $e)
{
	echo "I got an error! :)";
}

Protection crypt usage:

require HANDLER_PATH.'ProtectionHandler.php';
$encoded = ProtectionHandler::encode("MY STRING","MY PASSWORD FOR DECODING","MY SALT");
echo $encoded;
echo "<br/>";
$decoded = ProtectionHandler::decode($encoded,"MY PASSWORD FOR DECODING","MY SALT");
echo $decoded;

Templating system usage:
$parse = array(); // Set default
$parse['who'] = "InCube"; // My variable to parse in the system: eg: {who} will be parsed as InCube
echo PageHandler::parse( PageHandler::template('MY TEMPLATE'), $parse);