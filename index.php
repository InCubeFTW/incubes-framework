<?php
/**
* @author InCube
* @copyright 2013
*/
require 'app/bootstrap.php';
require MODEL_PATH.'index.php';
/**
* @des Define view controller
*/
class Page extends IndexModel
{
	public function __construct()
	{

		/**
		* Compile Model
		*/
		parent::__construct();

		/**
		* Make check, before missing functions and being crap.
		*/
		if(!class_exists('bootstrap') || !class_exists('PageHandler'))
		{
			echo "<h1>Error</h1>";
			echo "Some classes are missing!";
			exit;
		}

		/**
		* Templating system by Advocaite and edited by incube
		*/
		$parse = array();
		$parse['who'] = "InCube";
		echo PageHandler::parse( PageHandler::template('index'), $parse); 
		/*
		* PDO TESTS
		*/
/*		try
		{
			$stmt = bootstrap::$dbh->query("SELECT * FROM test");
			$fuu = $stmt->fetchAll(PDO::FETCH_ASSOC);
			var_dump($fuu);
		}
		catch(PDOException $e){}*/

	}
}

$page = new Page;