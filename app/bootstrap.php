<?php
/**
* @author InCube
* @developers 
* @des Main bootstrap class for RPG
*/
class bootstrap
{
	/**
	* @var $maintenace
	* @des Maintenace mode On/Off
	* @value 1 or 2
	*/
	public $maintenace = 0;

	/**
	* @var $dbh
	* @des Database handle
	*/
	static public $dbh;
	/**
	* @var $connection
	* @des Connection to database status. 0 or 1
	*/
	public $connection = 0;
	/**
	* @var $PDO_USER
	* @des Databse user
	*/
	static public $PDO_USER = 'root';

	/**
	* @var $PDO_Password
	* @des Database password
	*/
	static public $PDO_Password = '';

	/**
	* @var $PDO_DB
	* @des Database name
	*/
	static public $PDO_DB = 'testdb';

	/**
	* @des Construct the boot.
	*/
	public function __construct()
	{
		define( "ROOT_PATH", realpath( dirname( dirname( __FILE__ ) ) ).DIRECTORY_SEPARATOR );
		define( "APP_PATH", ROOT_PATH."app".DIRECTORY_SEPARATOR );
		define( "LIB_PATH", APP_PATH."lib".DIRECTORY_SEPARATOR );
		define( "MODEL_PATH", APP_PATH."model".DIRECTORY_SEPARATOR );
		define( "VIEW_PATH", APP_PATH."view".DIRECTORY_SEPARATOR );
		define( "HANDLER_PATH", APP_PATH."handler".DIRECTORY_SEPARATOR );

		require HANDLER_PATH.'PageHandler.php';

		if (version_compare(PHP_VERSION, '5.4.0') <= 0) {
			echo "<h1>PHP Version Error.</h1>";
			echo "I am sorry, but you have PHP ".PHP_VERSION.", but we require at least PHP 5.4.0 ";
			exit;
		}

		if(!isset($this->dbh) && $this->connection == 1){
			$this->PDO_Connection();
		}
	}

	/**
	* @des Make PDO connection with MySQL
	*/
	static public function PDO_Connection()
	{
		if(!isset(self::$dbh))
		{
			try
			{
				self::$dbh = new PDO ('mysql:host=localhost;dbname='.self::$PDO_DB,self::$PDO_USER, self::$PDO_Password);
				self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch (PDOException $e)
			{
				echo "<h1>Database connection failed</h1>";
				echo "Error message: ".$e->getMessage()."<br/>";
				exit;
			}
		}
	}
}
$boot = new bootstrap;
?>