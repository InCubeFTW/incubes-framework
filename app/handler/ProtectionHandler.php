 <?php
/**
 * @author InCube
 * @package D.A.F.E. Framework
 * @copyright 2013
 *
 * USAGE:
 * ProtectionHandler::encrypt('STRING','FIRST SALT','SECOND SALT')
 */
class ProtectionHandler
{
		public function __construct(){}
                static function encrypt($decrypted, $password, $salt = 'My salt is here bro!')
                {
                                $key = hash('SHA256', $salt . $password, true);
                                srand();
                                $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_RAND);
                                if (strlen($iv_base64 = rtrim(base64_encode($iv), '=')) != 22)
                                                return false;
                                $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $decrypted . md5($decrypted), MCRYPT_MODE_CBC, $iv));
                                return $iv_base64 . $encrypted;
                }
                static function decrypt($encrypted, $password, $salt = 'My salt is here bro!')
                {
                                $key       = hash('SHA256', $salt . $password, true);
                                $iv        = base64_decode(substr($encrypted, 0, 22) . '==');
                                $encrypted = substr($encrypted, 22);
                                $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($encrypted), MCRYPT_MODE_CBC, $iv), "\0\4");
                                $hash      = substr($decrypted, -32);
                                $decrypted = substr($decrypted, 0, -32);
                                if (md5($decrypted) != $hash)
                                                return false;
                                return $decrypted;
                }
}
/*
~~~~~~~~~~~~EXAMPLE~~~~~~~~~~~~~~~
echo "Hello world: ";
$x = ProtectionHandler::encrypt('Hello world','123123');
echo $x."<br/>Decoded:";
echo ProtectionHandler::decrypt($x,'123123');
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
?>