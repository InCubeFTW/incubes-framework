<?php
/**
* @author Advocaite
* @reworked InCube
* @copyright 2013
*/
class PageHandler
{
    public function __construct(){}
	/**
	* Parset template
	*/
	public static function parse($template, $array)
	{
    	foreach ($array as $a => $b)
    	{
        	$template = str_replace("{{$a}}", $b, $template);
    	}
    	return $template;
    }
    /**
    * Get template
    */

    public static function template($template)
    {
    	$filename = VIEW_PATH . $template . ".tpl";
    	$f = fopen($filename, "r") or die ( " Could not open template in ".$filename );
    	$content = fread($f, filesize($filename)) or die ( "Could not read template in ".$filename );
    	fclose ($f);
    	return $content;
    }
}